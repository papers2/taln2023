#!/bin/bash

input_dir=corpus_scrap_pdf
output_dir=corpus_xml
conf=jeptalnrecital

java -Xmx4G -jar grobid-0.7.2/grobid-core/build/libs/grobid-core-0.7.2-onejar.jar -gH grobid-0.7.2/grobid-home -dIn "$input_dir/$conf" -dOut "$output_dir/$conf" -r -exe processFullText

for p in "$output_dir/$conf/"*
do
  mv "$p/pdf/"* "$p"
  rmdir "$p/pdf"
done
