#!/bin/bash

wget -O grobid.zip https://github.com/kermitt2/grobid/archive/0.7.2.zip
unzip grobid.zip

cd grobid-0.7.2
./gradlew clean install
