#!/bin/python3

import json, glob
from bs4 import BeautifulSoup as bs
from pybtex.database.input import bibtex
from collections import Counter, defaultdict
import matplotlib.pyplot as plt
import pandas as pd
from collections import Counter
import numpy as np
import seaborn as sns


def add_missing_resource(resources, dic):
    for i in resources:
        if i not in dic:
            dic[i] = 0
    return dic

def use_type(df, use):
    resources = list(df['Ressource'].unique())
    df_use = df.loc[df['Type'] == use]
    use = add_missing_resource(resources, Counter(df_use['Ressource']))
    use = dict(sorted(use.items(), key=lambda i: i[0]))
    return use

def data_for_barplot(df):
    utilisation = use_type(df, 'Utilisation')
    comparison = use_type(df, 'Comparaison')
    construction = use_type(df, 'Construction')
    mention = use_type(df, 'Mention')

    data = {
        'Construction': list(construction.values()),
        'Utilisation': list(utilisation.values()),
        'Comparaison' : list(comparison.values()),
        'Mention': list(mention.values()),
    }

    resources_list = list(utilisation.keys())

    return data, resources_list

def filter_articles(data, resources_list, threshold):
    """
        Keep articles with at least a certain number (threshold) of occurrences
        Input : data from data_for_barplot
        Return : dataframe with occurrences for each type
    """
    data.update({"Ressource" : resources_list})     #add resources names
    df = pd.DataFrame.from_dict(data)
    df['somme'] = df.sum(axis="columns")
    df_ = df.loc[df["somme"] > threshold]
    return df_

def change_format(df):
    l = [df.columns[(df == 1.0).iloc[i]].values[0] for i in range(len(df))]
    df_ = df.copy()
    df_ = df_.drop(['Construction', 'Utilisation', 'Comparaison', 'Mention', 'Erreur' ], axis=1)
    df_['Type'] = l
    return df_

def draw_barplot(data, resources_list, export):
    sns.set(style="darkgrid")
    colors = ["tab:blue", "tab:green", "tab:red", "tab:orange"]

    x = np.arange(len(resources_list))  # the label locations
    width = 0.2 # the width of the bars
    multiplier = -1
    i = 0
    fig, ax = plt.subplots(figsize=(20, 10))

    #for key, (color, value) in data.items():
    for key, value in data.items():
        offset = width * multiplier
        rects = ax.bar(x + offset, value, width, label=key, color=colors[i])
        ax.bar_label(rects, padding=3, fontsize=16)
        multiplier += 1
        i += 1

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel("Nombre d'articles", fontsize=20)
    ax.set_xticks(x + width, resources_list, fontsize=20)
    plt.setp(ax.get_xticklabels(), rotation=30, horizontalalignment='right')
    ax.legend(loc='upper right', fontsize = 22)
    #ax.set_ylim(0, 38)
    ax.set_ylim(bottom=0)
    fig.set_tight_layout(True)

    if export:
        plt.savefig('figures/TALN_emb_type.png',  bbox_inches='tight')

    plt.show()

if __name__ == '__main__':

        # lexical resources barplot according to type of use

    df_res = pd.read_csv('data/TALN_res_annotated.csv')
    data_res, resources_list = data_for_barplot(df_res)
    #draw_barplot(data_res, resources_list, False)

        # main lexical resources barplot

    df_filtered = filter_articles(data_res, resources_list, 2)
    data_filtered = {
        'Construction': df_filtered['Construction'],
        'Utilisation': df_filtered['Utilisation'],
        'Comparaison' : df_filtered['Comparaison'],
        'Mention': df_filtered['Mention'],
    }
    # draw_barplot(data, df_filtered['Ressource'], False)

        # embeddings barplot according to type of use

    # df_emb = pd.read_csv('data/TALN_emb_annotated.csv')
    # df_emb = df_emb[["Annee", "ID", "Ressource",  "Annotateur", "Construction", "Utilisation", "Comparaison", "Mention", "Erreur"]]
    # df_emb = change_format(df_emb)

    # data_emb, resources_list = data_for_barplot(df_emb)
    # draw_barplot(data_emb, resources_list, False)
