#!/bin/bash

conf=jeptalnrecital

for year in {2001..2022};
do
  bash acl_anthology_scrap.sh --conf jeptalnrecital --year ${year}
done
