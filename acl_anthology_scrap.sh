
#!/bin/bash

function error {
        echo "$(basename "$0"): error: $1"
        exit 1
}

dir=corpus_scrap_pdf
conf=jeptalnrecital
year=2022

while [ "$#" -ge 1 ]; do
        arg="$1"
        shift

        if [ "$arg" = "-y" -o "$arg" = "--year" ]; then
                if [ $# -eq 0 ]; then
                        error "option \"$arg\" needs an argument."
                fi
                year="$1"
                shift
        elif [ "$arg" = "-c" -o "$arg" = "--conf" ]; then
                if [ $# -eq 0 ]; then
                        error "option \"$arg\" needs an argument."
                fi
                conf="$1"
                shift
        elif [ "$arg" = "-h" -o "$arg" = "--help" ]; then
                echo "Usage: $(basename "$0") [ARG]..."
                echo "Download papers from aclanthology in PDF"
                echo
                echo "  -h, --help       print this message"
                echo "  -y, --year YEAR  download papers of year YEAR"
                echo "  -c, --conf CONF  download papers of conference CONF"
                exit 0
        else
                error "unknown option \"$arg\"."
        fi
done

#créé un dossier temporaire pour stocker des fichiers temporaires
tmp_path="/tmp/$conf-$year"
mkdir -p "$tmp_path"

#créé dossier où seront téléchargés les PDF
output_path="$dir/$conf/$year/pdf"
mkdir -p "$output_path"

#créé dossier où seront téléchargés les bib
bib_path="$dir/$conf/$year/bib"
mkdir -p "$bib_path"

#Récupère l'url de la conf et de l'année voulues
url="https://aclanthology.org/events/$conf-$year"

#Récupère les pages bibs de la page
wget "$url" -O "$tmp_path/page.html"
if [ "$?" -ne 0 ]; then
  echo "The year $year doesn't exist for the conference $conf"
fi

#Récupère les liens des bibs
bibs="$(cat "$tmp_path/page.html" | tr " " "\n" | grep "volumes/.*\.bib" | cut -d= -f2)"

for bib in $bibs; do
  url="https://aclanthology.org${bib}"
  bib_file="$bib_path/$(basename "$bib")"
  wget "$url" -O "$bib_file"
  cat "$bib_file" | grep -E "@proceedings|@inproceedings|@article| *url *=" | cut -d'"' -f2 > "$tmp_path/url_list.txt"

  while read -r line; do
    echo $line | grep "@inproceedings\|@article"
    if [ "$?" -eq 0 ]; then
      read -r line
      echo "$line" | grep '/$'
      if [ "$?" -eq 0 ]; then
        line="$(echo $line | sed 's/\/$//')"
      fi
      echo "$line" | grep '\.pdf$'
      if [ "$?" -ne 0 ]; then
        url="$line.pdf"
      else
        url="$line"
      fi
      wget -c "$url" -O "$output_path/$(basename "$url")"
    else
      read -r line
    fi
  done < "$tmp_path/url_list.txt"
done
