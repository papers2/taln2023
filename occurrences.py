#!/bin/python3

import json, glob
from bs4 import BeautifulSoup as bs
from pybtex.database.input import bibtex
from collections import Counter, defaultdict
import matplotlib.pyplot as plt
import pandas as pd

TALN_PATH = "corpus_xml/talnrecital/*/*.xml"
TALN_BIB_PATH = "corpus_scrap_pdf/jeptalnrecital/*/bib/*"


LRE_MAP = ["tl_dvl2_ladl", "dicovalence", "lefff", "jeuxdemots", "lglex", "framenet", "lvf", "lexique-grammaire", "verbenet", "resyf", "wolf",
            "glàff", "dela", "flelex", "démonette"]
ORTOLANG = ["holinet", "rl-fr", "lexique4linguists", "vernom", "nomage", "morphalou", "prolex", "tlfphraseo", "marsalex", "vfrlpl", "lgerm"]
RES = LRE_MAP + ORTOLANG

EMBEDDINGS = ['word2vec', 'fasttext', 'glove']

def read_body(path):
    content = []
    # Read the XML file
    with open(path, "r", encoding= 'utf-8', errors='ignore') as file:
        # Read each line in the file, readlines() returns a list of lines
        content = file.readlines()
        # Combine the lines in the list into a string
        content = "".join(content)
        bs_content = bs(content, features="xml")
    return bs_content.find("body")

def clean_text(text):
    text = text.lower()
    text = text.replace("glaff", "glàff")
    text = text.replace("demonette", "démonette")
    text = text.replace("dicolecte", "dicollecte")
    text = text.replace("w2v", "word2vec")
    text = text.replace("verb∋net", "verbenet")
    punct = ".,()"
    for i in punct:
        text = text.replace(i, " ")
    return text

def get_authors_by_article(articles):
    """
    Input : List of articles
    Ouput : Title and authors
    """
    liste = []
    for path in glob.glob(TALN_BIB_PATH):
        article = path.split("/")[-1]
        #open a bibtex file
        parser = bibtex.Parser()
        bibdata = parser.parse_file(path)

        #loop through the individual references
        for bib_id in bibdata.entries:
            b = bibdata.entries[bib_id].fields
            for article in articles:
                if article[:-8] == b['url'][25:]:  #article reference in url
                    # print("--------------")
                    # print(article)
                    l = [author for author in bibdata.entries[bib_id].persons["author"]]
                    liste.append(l)
    return liste

if __name__ == '__main__':

    data = []
    for path in glob.glob(TALN_PATH):
        year = path.split('/')[-2]
        article = path.split("/")[-1]
        content = read_body(path)
        if content == None:
            pass
        else:
            vocab = clean_text(content.get_text()).split()
            #for res in EMBEDDINGS:
            for res in RES:
                if res in vocab:
                    data.append({'Annee' : year, 'ID' : article, 'Ressource': res, 'Occ' : vocab.count(res)})

    df = pd.DataFrame(data)

    # auteurs =  get_authors_by_article(list(df['Article']))
    # df['Auteurs'] = auteurs

    print(df)
    df.to_csv('data/TALN_emb.csv', index=False, sep ='\t')
