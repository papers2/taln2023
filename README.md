# Des ressources lexicales du français et de leur utilisation en TAL : étude des actes de TALN

Statistics on French lexical resources mentioned in TALN between 2001 and 2022.

## Scrapping PDF corpus

We use `acl_anthology_scrap.sh` [ACL anthology](https://aclanthology.org/) to get all the papers of a particular conference in PDF. The proceedings are excluded. `scrap_multiple_years.sh` runs the precious script on multiple years.

**Parameters**
- `--conf` : name of the conference (jeptalnrecital)
- `--year` : year of the conference

## Convert PDF corpus to XML with GROBID

We use [GROBID](https://grobid.readthedocs.io/en/latest/) to convert our PDF in XML. `install_grobid.sh` installs GROBID 0.7.2 and we use JAVA version 11.
GROBID was installed in [batch mode](https://grobid.readthedocs.io/en/latest/Grobid-batch/).

`java -Xmx4G -jar grobid-core/build/libs/grobid-core-0.7.1-onejar.jar -gH grobid-home -dIn /path/to/input/directory -dOut /path/to/output/directory -r -exe processFullText `

`pdftoxml_grobid.sh` runs the command for several years of a conference.

We removed manually papers from JEP, workshops, tutorials and invited speakers.

## Check XML corpus

`check_corpus.py` checks the articles languages (Papers are written in English or French in TALN conference) with the tag `xml:lang`. A tag other than fr or en usually shows a problem in the conversion. The articles with conversion problems are removed. 

## Statistics on corpus

`stats.py` gives a csv file `TALN_res.csv` with each article and occurrences of each resource mentioned:

| Annee |                  ID                  |  Ressource | Occ |
|:-----:|:------------------------------------:|:----------:|:---:|
| 2017  | 2017.jeptalnrecital-court.6.tei.xml  | framenet   | 4   |
| 2017  | 2017.jeptalnrecital-court.13.tei.xml | wolf       | 1   |
| 2017  | 2017.jeptalnrecital-long.11.tei.xml  | jeuxdemots | 5   |

It also generates `TALN_emd.csv` for articles that mention embeddings:

| Annee |                   ID                  | Ressource | Occ |
|:-----:|:-------------------------------------:|:---------:|:---:|
| 2017  | 2017.jeptalnrecital-court.22.tei.xml  | word2vec  | 7   |
| 2017  | 2017.jeptalnrecital-recital.2.tei.xml | word2vec  | 22  |
| 2017  | 2017.jeptalnrecital-long.7.tei.xml    | glove     | 8   |
