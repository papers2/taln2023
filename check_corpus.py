#!/bin/python3

import json, glob
from bs4 import BeautifulSoup
import lxml

TALN_PATH = "corpus_xml/taln/*/*.xml"

def read_file(path):
    with open(path, encoding='utf-8', errors='ignore') as f:
        file = f.read()
    return file

def check_language(corpus_path, language):
    art_problem = []
    for path in glob.glob(corpus_path):
        article = path.split("/")[-1]
        content = read_file(path).lower()
        soup = BeautifulSoup(content, "xml")
        for lang in soup.find_all('text'):
            if lang.get("xml:lang") != language:
                art_problem.append(article)
                # print(article)
                # print(lang.get("xml:lang"))
                # for line in soup.find_all("body"):
                #     print(line.text[:10])
    return art_problem

def check_article(corpus_path):
    art_problem = []
    for path in glob.glob(corpus_path):
        article = path.split("/")[-1]
        content = read_file(path).lower()
        soup = BeautifulSoup(content, "xml")
        for line in soup.find_all("body"):
            body = line.text
            #if "the" not in body and "le" not in body:
            if "le" not in body:
                art_problem.append(article)
                #print(article)
    return art_problem

if __name__ == '__main__':

    art_lang_pb = check_language(TALN_PATH, "fr")
    art_content_pb = check_article(TALN_PATH)

    print(art_lang_pb)
    print("balise langue incorrecte :", len(art_lang_pb))
    print(art_content_pb)
    print("contenu mal converti :", len(art_content_pb))


    print(set(art_content_pb).issubset(set(art_lang_pb)))
    pbs = set(art_content_pb)-set(art_lang_pb)
    print(pbs)
